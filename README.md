# Сайт idacb.com

Это django-приложение.

Доставка на боевой сервер происходит с помощью Gitlab CI в два этапа, автоматически на тестовый сервер (staging) и, с ручной инициацией, на production (см. .gitlab-ci.yml).

Работа приложения на сервере осуществлена с помощью docker (см. Dockerfile, .gitlab-ci.yml).

## Разработка
```
git clone git@gitlab.com:idacb/idacb.com.git
cd idacb.com
docker-compose run --rm --entrypoint ' ' app ./manage.py makemigrations
docker-compose up
```
Результат: http://localhost

## Хостинги

База данных и загруженые файлы (media) хранятся на обычном хостинге. 

Само приложение независимо работает на любом сервере, например обычном VDS, на котором настроены docker и gitlab-runner в режиме shell ([Установка gitlab-runner](https://docs.gitlab.com/runner/install/linux-manually.html)). 

Адреса и доступы настраиваются с помощью ENV и Secret Variables в GitLab (см. .gitlab-ci.yml, см. Settings -> CI/CD -> Secret Variables)