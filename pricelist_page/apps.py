from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class PricelistPageConfig(AppConfig):
    name = 'pricelist_page'
    verbose_name = _('page "pricelist"')