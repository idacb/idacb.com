from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import HttpResponse, Http404
from django.core.mail import send_mail
from django.conf import settings

class HomePageView(TemplateView):
    template_name = 'pricelist_page/home.html'