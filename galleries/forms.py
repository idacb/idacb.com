from django import forms
from django.db import models
from django.utils.translation import gettext as _

from django.forms.widgets import HiddenInput

from .models import Picture
from .widgets import PictureInput

class PictureForm(forms.ModelForm):
    DELETE = forms.BooleanField(label=_('delete'))
    class Meta:
        model = Picture
        fields = ['file', 'position']
        widgets = {
            'file': PictureInput,
            'position': HiddenInput
        }
        labels = {
            'file': ''
        }