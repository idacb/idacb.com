Installation:
1) Add 'galleries.apps.GalleriesConfig' in INSTALLED_APPS
2) Add galleries.models.PictureConsumerMixin to your model parent classes
3) Add galleries.admin.PictureInline to your model's ModelAdmin.inlines list
[4) Add galleries.admin.PictureConsumerAdminMixin to your ModelAdmin parent classes to have preview column in changelist (also need to add 'preview' in ModelAdmin.list_display list)]