;(function(jQuery){

	const $ = jQuery;

	window.galDismissPopup = function(win) {
		win.close();
	}

	$(document).ready(function() {

		// Inlines logic

		$(".gal-js-inline").each(function() {

			const $inline = $(this);



			// Files upload

			const $list = $inline.find('.gal-js-list'),
				  $input = $inline.find('.gal-js-input'),
				  $progress = $inline.find('.gal-js-progress'),
				  $progressDisplay = $inline.find('.gal-js-progress__display'),
				  data = $inline.data(),
				  options = data.inlineFormset.options;

			$inline.find('.gal-js-add-button').on('click', function(e) {
				e.preventDefault();
				$input.click();
			});

			$input.on('change', function(e) {

				const files = e.currentTarget.files;
				if (files.length > 0) {

					const data = new FormData();

					data.append('csrfmiddlewaretoken', $input.data('csrfmiddlewaretoken'));
					data.append('object_id', $input.data('object-id'));
					data.append('content_type', $input.data('content-type-id'));
					data.append('prefix', options.prefix);
					data.append('form-TOTAL_FORMS', files.length)
					data.append('form-INITIAL_FORMS', 0)
					data.append('form-MAX_NUM_FORMS', '')
					data.append('form-MIN_NUM_FORMS', '')

					Array.from(files).forEach(function(file, i) {
						data.append(`form-${i}-file`, file);
					})

					$.ajax({
						url: $input.attr('action'),
						type: 'POST',
						data: data,
						cache: false,
				        contentType: false,
				        processData: false,
				        xhr: function() {
				        	const xhr = new window.XMLHttpRequest();
				        	xhr.upload.addEventListener('progress', function(e) {
				        		if (e.lengthComputable) {
				        			const complete = (e.loaded / e.total).toFixed(2) * 100 + '%';
				        			$progressDisplay.html(complete)
				        		}
				        	})
				        	return xhr;
				        },
				        beforeSend: function() {
				        	$progress.show();
				        },
				        success: function(data) {
							$list.html(data);
						},
						complete: function() {
							$progress.hide();
						}
					});
					$input.val('');
				}
			})



			// Pictures drag and drop

			var $dragged = null;

			function updatePositionFields() {
				$inline.find('.gal-js-item').each(function(i) {
					$(this).find('input[name$=position]').val(i + 1);
				});
			}

			$(document).on('dragstart', '.gal-js-item', function(e) {
				e.originalEvent.dataTransfer.setData('text/plain', 'firefox fix');
				$dragged = $(this);
				$dragged.addClass('gal-item_muted');
			});

			$(document).on('dragend', '.gal-js-item', function(e) {
				$dragged.removeClass('gal-item_muted');
				$dragged = null;
			});

			$(document).on('dragover', '.gal-js-item', function(e) {
				const $underlay = $(this);
				if (!!$dragged && !$underlay.is($dragged)) {
					if ($underlay.nextAll().filter($dragged).length) {
						$dragged.insertBefore($underlay);
					} else {
						$dragged.insertAfter($underlay);
					}
					updatePositionFields();
				}
			});

			// Mobile arrow buttons

			$inline.find('.gal-js-item').each(function() {

				const $item = $(this);

				$item.find('.gal-js-move-left').on('click', function(e) {
					const $items = $inline.find('.gal-js-item');
					if ($item.is($items.first())) {
						$item.insertAfter($items.last());
					} else {
						$item.insertBefore($item.prev());
					}
					updatePositionFields();
				});
				$item.find('.gal-js-move-right').on('click', function(e) {
					const $items = $inline.find('.gal-js-item');
					if ($item.is($items.last())) {
						$item.insertBefore($items.first());
					} else {
						$item.insertAfter($item.next());
					}
					updatePositionFields();
				});

			});



			// Dropzone

			const $dropzone = $inline.find('.gal-js-dropzone'),
				  $dropzoneArea = $inline.find('.gal-js-dropzone__area');

			var filesDragLastTarget = null;

			// Prevent browser from opening files
			$(window).on('dragover', function(e) {
				e.preventDefault();
			});
			$(window).on('drop', function(e) {
				e.preventDefault();
				// And hide dropzone
				$dropzone.hide();
			});

			// Dropzone showing
			$(document).on('dragenter', function(e) {
				e.preventDefault();
				var containsFiles = false;
				const dataTransfer = e.originalEvent.dataTransfer;
				if (dataTransfer.types) {
				    for (var i=0; i < dataTransfer.types.length; i++) {
				        if (dataTransfer.types[i] == "Files") {
				            containsFiles = true;
				        }
				    }
				}
				if (containsFiles) {
				    $dropzone.show();
					filesDragLastTarget = e.target;
				}
			});
			$(document).on('dragleave', function(e) {
				e.preventDefault();
				if (e.target === filesDragLastTarget || e.target === document) {
					$dropzone.hide();
				}
			});

			// Dropzone highlighting
			$dropzoneArea.on('dragenter', function(e) {
				$dropzone.addClass('gal-dropzone_active');
			})
			$dropzoneArea.on('dragleave', function(e) {
				$dropzone.removeClass('gal-dropzone_active');
			})

			// Dropping files
			$dropzoneArea.on('drop', function(e) {
				$input.get(0).files = e.originalEvent.dataTransfer.files;
				$input.trigger('change')
			})

		});

	});

})(django.jQuery)