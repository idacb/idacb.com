from django.forms.widgets import ClearableFileInput

class PictureInput(ClearableFileInput):
	template_name = 'galleries/picture_input.html'