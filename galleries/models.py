import os

from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType

from django.utils.translation import ugettext_lazy as _

from django.contrib.staticfiles.templatetags.staticfiles import static

from sorl.thumbnail import get_thumbnail

# Create your models here.

class Picture(models.Model):
    file = models.ImageField(upload_to='galleries_pictures', verbose_name=_('file'))
    title = models.CharField(max_length=255, blank=True, verbose_name=_('title'), help_text=_('Title for pictures is needed by SEO standarts'))
    alt = models.CharField(max_length=255, blank=True, verbose_name=_('alt text'), help_text=_('Alt text for pictures is needed by SEO standarts'))
    position = models.PositiveIntegerField(blank=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    @property
    def filename(self):
        return os.path.basename(self.file.name)

    def save(self, *args, **kwargs):
        if not self.position and not 'position' in kwargs:
            query_set = Picture.objects.filter(content_type = self.content_type, object_id = self.object_id)
            self.position = query_set.latest('position').position + 1 if query_set else 1
        super().save(*args, **kwargs)

    def __str__(self):
        return self.file.name

    class Meta:
        ordering = ['position']
        verbose_name = _('picture')
        verbose_name_plural = _('pictures')

class PictureConsumerMixin(models.Model):
    pictures = GenericRelation(Picture)
    @property
    def thumbnail_url(self):
        url = static('galleries/no_picture.svg')
        if self.pictures.count() > 0:
            thumbnail = get_thumbnail(self.pictures.first().file, '150x150', crop='center', quality=100)
            url = thumbnail.url
        return url
    class Meta:
        abstract = True