from django.contrib import admin
from django.urls import path

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.admin import GenericInlineModelAdmin
from django.contrib.contenttypes.forms import generic_inlineformset_factory

from django.forms.utils import ErrorDict
from django.http import HttpResponse, Http404
from django.core import serializers
from django.shortcuts import render

from django.utils.translation import ugettext_lazy as _
from django.utils.html import format_html

from django.contrib.staticfiles.templatetags.staticfiles import static
from sorl.thumbnail import get_thumbnail

from .models import Picture
from .forms import PictureForm
from .widgets import PictureInput



class PictureInline(GenericInlineModelAdmin):
    template = 'galleries/inline.html'
    model = Picture
    form = PictureForm
    extra = 0

    def no_parent_message(self):
        return _('save %(parent)s first to add pictures.') % {'parent': self.parent_model._meta.verbose_name}

    class Media:
        css = {'all': ('galleries/styles.css',)}
        js = ('galleries/script.js',)



class PictureAdmin(admin.ModelAdmin):

    fields = ('file', 'title', 'alt')
    readonly_fields = ('file',)

    # Hide images list, but make it available to edit
    def get_model_perms(self, request):
        return {}

    def get_urls(self):
        urls = super().get_urls()
        additional_urls = [
            path('upload/', self.admin_site.admin_view(self.upload), name='galleries_picture_upload'),
        ]
        return additional_urls + urls

    def upload(self, request):

        if request.method == 'POST':

            PictureFormset = generic_inlineformset_factory(Picture, form=PictureForm, extra=0)
            parent_instance = ContentType.objects.get_for_id(request.POST['content_type']).get_object_for_this_type(id=request.POST['object_id'])
            add_formset = PictureFormset(request.POST, request.FILES, parent_instance, prefix='form')
            errors = None

            if add_formset.is_valid():
                add_formset.save()
            else:
                errors = []
                for form in add_formset:
                    if form.errors:
                        message = '"%s": ' % form.files['%s-file' % form.prefix].name
                        for field_name, field_messages in form.errors.items():
                            message += ' '.join(field_messages)
                        errors.append(message)

            list_formset = PictureFormset(instance=parent_instance, prefix=request.POST['prefix'])
            # import pdb; pdb.set_trace()
            list_formset._non_form_errors = list_formset.error_class(errors)

            return render(request, 'galleries/formset.html', context={'formset': list_formset})

        else:
            raise Http404()

    def response_change(self, request, obj):
        """
        Overriding to force the widget to update
        """
        resp = super().response_change(request, obj)
        if '_popup' in request.POST:
            return HttpResponse('<script type="text/javascript">opener.galDismissPopup(window);</script>')
        return resp

admin.site.register(Picture, PictureAdmin)



class PictureConsumerAdminMixin():
    def preview(self, obj):
        return format_html(
            '<img src="{}" style="width: 50px; height: 50px; border-radius: 4px">',
            obj.thumbnail_url
        )
    preview.short_description = ''