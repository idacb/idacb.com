"""my_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf import global_settings
from django.conf.urls.static import static

from common.views import RobotsView

urlpatterns_static = static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns_media = []
# Media shouldn't be served locally if it's not on the app's host
if global_settings.DEFAULT_FILE_STORAGE == settings.DEFAULT_FILE_STORAGE:
    urlpatterns_media += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns_debug = []
if settings.DEBUG:
    import debug_toolbar
    urlpatterns_debug = [path('__debug__/', include(debug_toolbar.urls))]

urlpatterns_robots = [path('robots.txt', RobotsView.as_view())]

urlpatterns_common = urlpatterns_static + urlpatterns_media + urlpatterns_debug + urlpatterns_robots

app_name = 'default'
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('webapp.urls'))
]
urlpatterns += urlpatterns_common