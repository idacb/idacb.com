from django.contrib import admin
from .models import ICO

@admin.register(ICO)
class ICOAdmin(admin.ModelAdmin):
    list_display = ('name', 'link', 'position')
    list_editable = ('position',)