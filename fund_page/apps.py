from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class FundPageConfig(AppConfig):
    name = 'fund_page'
    verbose_name = _('Page "Fund"')