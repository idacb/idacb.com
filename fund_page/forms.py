from django import forms
from django.utils.translation import ugettext_lazy as _

from django.urls import reverse_lazy

from common.forms import Form

class FundForm(Form):
    name = forms.CharField(max_length=255, required=False, label=_("Your name"))
    surname = forms.CharField(max_length=255, required=False, label=_("Your surname"))
    contact = forms.CharField(max_length=255, required=False, label=_("Your contact information"))

    class Meta(Form.Meta):
        title = 'Fund'
        class_name = 'form-control'
        action = reverse_lazy('apply')
        placeholders = {
            'name': 'John',
            'surname': 'Doe',
            'contact': '+1 123 456 78 90, johndoe@example.com',
        }