from django.urls import path, include
from django.conf import settings

from urls import urlpatterns_common

from django.contrib.sitemaps.views import sitemap
from .sitemaps import StaticViewSitemap

from django.contrib.auth import views as auth_views

from .views import HomePageView, IrPageView, FundLoginView
from common.views import ContactFormView
from .forms import FundForm

sitemaps = {'static': StaticViewSitemap}
urlpatterns_sitemap = [path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap')]

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('accounts/login/', FundLoginView.as_view(), name='login'),
    path('ir/', IrPageView.as_view(), name='ir'),
    path('apply/', ContactFormView.as_view(email_to=settings.APP_EMAIL_FUND, form_class=FundForm), name='apply'),
]
urlpatterns += urlpatterns_common
urlpatterns += urlpatterns_sitemap