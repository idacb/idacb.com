from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.shortcuts import resolve_url

from django.contrib.auth.views import LoginView

from .forms import FundForm
from .models import ICO

class HomePageView(TemplateView):
    template_name = 'fund_page/base.html'
    def get_context_data(self):
        icos = ICO.objects.all()
        return {
            'form': FundForm(),
            'icos': icos
        }

class IrPageView(LoginRequiredMixin, TemplateView):
    template_name = 'fund_page/ir.html'

class FundLoginView(LoginView):
    template_name = 'fund_page/login.html'
    redirect_url_name_default = 'ir'
    def get_success_url(self):
        url = self.get_redirect_url()
        return url or resolve_url(self.redirect_url_name_default)