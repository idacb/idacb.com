;$(function(){
	var timeFormat = 'MMMYYYY';
	var canvas = document.getElementById("chart").getContext('2d');
	var configChart = {
	    type: 'line',
	    data: {
	        labels: [
	        	'JAN2017','FEB2017','MAR2017','APR2017','MAY2017','JUNE2017','JULY2017','AUG2017','SEP2017',
	        	'OCT2017','NOV2017','DEC2017','JAN2018','FEB2018','MAR2018','APR2018','MAY2018','JUN2018'
	        ],
	        datasets: 
	        [
		        {
		        	label: 'IDACB Top Crypto Currency Fund',
		            backgroundColor: '#FFC000',
		            borderColor: '#FFC000',
		            data: [
		            	100.00,114.54,150.19,292.99,736.18,1428.45,1367.05,1473.43,2572.63,2042.60,
		            	2058.60,3975.81,14619.09,7094.77,6537.70,3596.53,7417.29,5465.20
		            ],
		            fill: false,
		            lineTension: 0
		        },
		        {
		        	label: 'IDACB Top ICO Fund',
		            backgroundColor: '#FFF',
		            borderColor: '#FFF',
		            data: [
		            	100.00,138.88,138.56,310.67,558.26,1013.05,1123.03,1136.55,1485.48,1120.64,
		            	808.54,1509.26,4641.57,3028.92,2723.16,1639.89,3485.52,2711.94
		            ],
		            fill: false,
		            lineTension: 0
		        },
		        {
		        	label: 'BTC buy and hold',
		            backgroundColor: 'rgb(6, 166, 235)',
		            borderColor: 'rgb(6, 166, 235)',
		            data: [
		            	100.00,108.08,129.64,112.08,163.98,260.12,251.90,333.80,480.99,448.40,
		            	771.51,1160.65,1778.84,943.63,1171.27,716.02,1012.54,797.73
		            ],
		            fill: false,
		            lineTension: 0
		        },
		        {
		        	label: 'Typical WeighedCAP Fund',
		            backgroundColor: '#ED7D31',
		            borderColor: '#ED7D31',
		            data: [
		            	100.00,108.90,132.50,138.99,236.49,436.58,437.64,490.37,717.46,634.11,
		            	931.23,1458.53,3060.60,1614.67,1731.81,973.71,1612.43,1224.95
		            ],
		            fill: false,
		            lineTension: 0
		        },
	        ]
	    },
	    options: {
	    	responsive: true,
	    	elements: {
                point:{
                    radius: 0
                }
            },
	    	tooltips: {
				mode: 'index',
				intersect: false,
				callbacks: {
	                label: function(tooltipItem, data) {
	                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

	                    if (label) {
	                        label += ': ';
	                    }
	                    label += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + '%';
	                    return label;
	                }
	            }
			},
            scales: {
                xAxes: [{
                	type: 'time',
                	time: {
                		parser: timeFormat,
                	},
					ticks: {
						fontColor: '#fff'
					},
					gridLines: {
	            	    display: false,
	            	    color: '#777'
	                },
                }],
                yAxes: [{
	                ticks: {
	                	fontColor: '#fff',
	                	callback: function(value) {
							return value + "%"
						},
	                },
	                gridLines: {
	            	    color: '#777'
	                },
            	}]
            },
        	maintainAspectRatio: false,
			legend: {
				onClick: function (e) {
				    e.stopPropagation();
				},
				position: 'bottom',
				labels: {
					fontColor: '#fff'
				}
			},
        }
	};

	var chart = new Chart(canvas, configChart);
});