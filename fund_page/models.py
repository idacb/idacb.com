from django.db import models
from webapp.models import PartnerAbstarct

from django.utils.translation import ugettext_lazy as _

class ICO(PartnerAbstarct):
    logo = models.ImageField(upload_to='fund_page/ico/logo')
    class Meta:
        verbose_name = _('Fund invested ICO')
        verbose_name_plural = _('Fund invested ICOs')