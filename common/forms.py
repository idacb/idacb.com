from django import forms

class TrumbowygPictureForm(forms.Form):
    picture = forms.ImageField()

class Form(forms.Form):


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for bounded_field in self:
            bounded_field.field.widget.attrs['class'] = self.Meta.class_name
            bounded_field.field.widget.attrs['class'] += ' %s' % self.Meta.class_name_error if bounded_field.errors else ''
        for field_name, placeholder in self.Meta.placeholders.items():
            if self.fields.get(field_name):
                self.fields.get(field_name).widget.attrs['placeholder'] = placeholder

    def as_div(self):
        return self._html_output(
            normal_row='<div class="%s %%(css_classes)s">%%(label)s%%(errors)s%%(field)s%%(help_text)s</div>' % self.Meta.class_name_group,
            error_row='<div>%s</div>',
            row_ender='</div>',
            help_text_html='<div class="helptext">%s</div>',
            errors_on_separate_row=False,
        )

    def __str__(self):
        return self.as_div()

    class Meta:
        placeholders = {}
        class_name = 'form-control'
        class_name_error = 'is-invalid'
        class_name_group = 'form-group'
        action = '/'
        title = 'Contact'