from django.db import models
from django.utils import timezone

from django.utils.translation import ugettext_lazy as _

class ResourceMixin(models.Model):
    created_at = models.DateTimeField(editable=False, null=True, verbose_name=_('created at'))
    updated_at = models.DateTimeField(editable=False, null=True, verbose_name=_('updated at'))
    is_active = models.BooleanField(default=True, verbose_name=_('is active'))

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super().save(*args, **kwargs)

    class Meta:
        abstract = True

class SlugMixin(models.Model):
    slug = models.SlugField(unique=True, verbose_name=_('slug'), help_text=_('Slug is used in page url. It looks like: http://.../page_slug/'))
    class Meta:
        abstract = True