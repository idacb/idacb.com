from django.shortcuts import render
from django.utils.six.moves.urllib.parse import urlsplit
from django.views.generic import TemplateView, FormView
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.core.mail import send_mail

class RobotsView(TemplateView):
    template_name = 'common/robots.txt'
    content_type='text/plain'
    def get_context_data(self):
        context = {}
        path = None
        try:
            path = reverse('django.contrib.sitemaps.views.sitemap')
        except:
            pass
        if path:
            scheme = urlsplit(self.request.build_absolute_uri(None)).scheme
            host = self.request.get_host()
            sitemap_url = '%s://%s%s' % (scheme, host, path)
            context = {'sitemap_url': sitemap_url}
        return context


class FormPostView(FormView):
    http_method_names = ['post']
    template_name = 'common/form.html'
    def form_valid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

class ContactFormView(FormPostView):
    email_to = settings.EMAIL_TO_DEFAULT
    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            user_email = form.cleaned_data['email'] if 'email' in form.cleaned_data else ''
            subject = _('%(form_title)sForm is applied. %(user_email)s') % {'user_email': user_email, 'form_title': '%s ' % form.Meta.title}
            email_from = settings.EMAIL_FROM
            email_to = self.email_to
            message = ''
            for field, value in form.cleaned_data.items():
                message += '%s:\n%s\n\n' % (form[field].field.label, value or '- %s -' % _('not specified'))
            try:
                send_mail(subject, message, email_from, [email_to])
            except:
                form.add_error(None, _("An error occurred during form processing. Please mail us to %s") % settings.EMAIL_TO_DEFAULT)
                return self.form_invalid(form)
            return self.form_valid(form)
        else:
            return self.form_invalid(form)