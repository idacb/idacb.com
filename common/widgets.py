from django.forms.widgets import Textarea
from django.utils.safestring import mark_safe
from django.utils.translation import get_language, get_language_info
from django.urls import reverse
from django.contrib.staticfiles.templatetags.staticfiles import static

def trumbowyg_widget_factory(url="trumbowyg/upload/"):
    class TrumbowygWidget(Textarea):
        upload_url = url
        class Media:
            css = {
                'all': (
                    'trumbowyg/dist/ui/trumbowyg.min.css',
                    'trumbowyg/styles.css',
                )
            }
            js = (
                '//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js',
                'trumbowyg/dist/trumbowyg.min.js',
                'trumbowyg/dist/plugins/upload/trumbowyg.upload.min.js',
                'trumbowyg/dist/plugins/table/trumbowyg.table.min.js',
                'trumbowyg/dist/plugins/pasteimage/trumbowyg.pasteimage.min.js',
                'trumbowyg/dist/plugins/cleanpaste/trumbowyg.cleanpaste.min.js',
                'trumbowyg/dist/plugins/fontsize/trumbowyg.fontsize.js',
            )
        def render(self, name, value, attrs=None):
            output = super().render(name, value, attrs)
            script = '''
                <script>
                    var csrf = $('[name="csrfmiddlewaretoken"]').attr('value')
                    $.trumbowyg.svgPath = '%s';
                    $("#id_%s").trumbowyg({
                        resetCss: true,
                        btnsDef: {
                            customFormatting: {
                                dropdown: ['h2', 'h3', 'h4', 'blockquote', 'p'],
                                title: 'Formatting',
                                ico: 'p',
                                hasIcon: true
                            }
                        },
                        btns: [
                            ['viewHTML'],
                            ['undo', 'redo'], // Only supported in Blink browsers
                            ['formatting'],
                            ['table'],
                            ['fontsize'],
                            ['strong', 'em', 'del'],
                            ['superscript', 'subscript'],
                            ['link'],
                            ['insertImage', 'upload'],
                            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                            ['unorderedList', 'orderedList'],
                            ['horizontalRule'],
                            ['removeformat'],
                            ['fullscreen']
                        ],
                        plugins: {
                            upload: {
                                data: [{name: 'csrfmiddlewaretoken', value: csrf}],
                                fileFieldName: "picture",
                                serverPath: "%s",
                            },
                            table: {

                            }
                        }
                    });
                </script>
            ''' % (static('trumbowyg/dist/ui/icons.svg'), name, self.upload_url)
            output += mark_safe(script)
            return output
    return TrumbowygWidget

TrumbowygWidget = trumbowyg_widget_factory()