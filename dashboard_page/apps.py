from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class DashboardPageConfig(AppConfig):
    name = 'dashboard_page'
    verbose_name = _('page "dashboard"')