FROM python:3

RUN apt-get update && apt-get install -y gettext vim

WORKDIR /usr/src/app

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

ENTRYPOINT [ "./docker-entrypoint.sh" ]

CMD [ "python", "manage.py", "runserver", "0.0.0.0:80" ]