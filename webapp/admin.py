from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from django.utils.html import format_html
from constance.admin import ConstanceAdmin, Config

from .models import Person, Party, Country, Association, Partner, Company, CountryDirectored, Event, Document

@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    autocomplete_fields = ('country',)
    list_display = ('thumbnail', 'name', 'link', 'country')
    list_editable = ('link',)
    list_filter = ('parties', 'country')
    list_display_links = ('thumbnail', 'name')
    search_fields = ('name', 'country__name')
    def thumbnail(self, obj):
        return format_html(
            '<img src="{}" style="width: 50px; border-radius: 4px">',
            obj.picture.url
        )
    thumbnail.short_description = ''

@admin.register(Party)
class PartyAdmin(admin.ModelAdmin):
    search_fields = ('name',)

@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_past', 'position')
    list_editable = ('is_past', 'position')
    search_fields = ('name',)

@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    list_display = ('thumbnail', 'name')
    list_display_links = ('thumbnail', 'name')
    def thumbnail(self, obj):
        return format_html(
            '<img src="{}" style="width: 50px; border-radius: 4px">',
            obj.flag.url if obj.flag else ''
        )
    thumbnail.short_description = _('flag')

@admin.register(Association)
class AssociationAdmin(admin.ModelAdmin):
    autocomplete_fields = ('country',)
    search_fields = ('name', 'url', 'country__name')
    list_filter = ('country',)
    list_display = ('name', 'url', 'country')

@admin.register(Partner)
class PartnerAdmin(admin.ModelAdmin):
    list_display = ('name', 'link', 'position')
    list_editable = ('position',)

@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'link', 'position')
    list_editable = ('position',)

@admin.register(CountryDirectored)
class CountryDirectoredAdmin(admin.ModelAdmin):
    list_display = ('country', 'director', 'joined')
    list_editable = ('joined',)
    autocomplete_fields = ('country', 'director')
    search_fields = ('country__name', 'director__name')

@admin.register(Document)
class CompanyAdmin(admin.ModelAdmin):
    pass