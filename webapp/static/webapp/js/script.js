;$(document).ready(function(){
	// Modals
	$(document).on('click', '.g-js-modal-open', function(e) {
		e.preventDefault();
		const modalId = $(this).data('id');
		$(`#${modalId}`).show(300);
		$('body').addClass('g-noscroll')
	})
	$(document).on('click', '.g-js-modal__close', function(e) {
		$(this).closest('.g-js-modal').hide(300);
		$('body').removeClass('g-noscroll');
	})
	// Scroll to
	$(document).on('click', '.g-js-scroll-to', function(e) {
		e.preventDefault();
		const $mobileMenu = $('.g-mobile-menu'),
			  $fixedMenu = $('.g-fixed-menu'),
			  id = $(this).data('id');
			  
		var offset = $(`#${id}`).offset().top;

		if ($mobileMenu.length && $mobileMenu.is(':visible')) {
			offset = offset - $mobileMenu.find('.navbar').outerHeight();
			const $toggler = $('.navbar-toggler');
			if (!$toggler.hasClass('collapsed')) {
				$toggler.click()
			}
		} else if ($fixedMenu.length){
			offset = offset - $fixedMenu.outerHeight();
		}
		$('.g-js-modal').hide(300);
		$('body').removeClass('g-noscroll');
		history.pushState({}, '', `${location.pathname}#${id}`)
	    $('html, body').animate({
	        scrollTop: offset + 5
	    }, 400);
	});
	// Form submit
	const FORM_SELECTOR = '.g-js-form',
		  FORM_CLASS_LOADING = 'g-form_loading'
	$(document).on('submit', `${FORM_SELECTOR} form`, function(e) {
		e.preventDefault;
		const $form = $(this),
			  $formContainer = $form.closest(FORM_SELECTOR);
			  
		$.post({
	        url: $form.attr('action'),
	        data: $form.serialize(),
	        beforeSend: function() {
	        	$formContainer.addClass(FORM_CLASS_LOADING)
	        },
	        success: function(data){
	        	$formContainer.replaceWith(data);
	        }
	    });
		return false
	})
	// Top desktop-menu
	$(window).scroll(function(e){
		const $menu = $('.g-js-fixed-menu'),
			  classShow = 'g-fixed-menu_show',
			  conditionHeight = $(window).outerHeight() - $menu.outerHeight() - 10;

		if (window.scrollY >= conditionHeight && !$menu.hasClass(classShow)) {
			$menu.addClass(classShow)
		} else if (!(window.scrollY >= conditionHeight) && $menu.hasClass(classShow)) {
			$menu.removeClass(classShow)
		}
	})

	// Apply button
	const $applyButton = $('.g-js-apply-button')
	if ($applyButton.length){
		$(window).scroll(function(e){
			const classShow = 'g-apply_show',
				  $applyScreen = $('#apply'),
				  isOnHeader = window.scrollY < $(window).outerHeight(),
				  isOnForm = window.scrollY > $applyScreen.offset().top;

			if ((isOnHeader || isOnForm) && $applyButton.hasClass(classShow)) {
				$applyButton.removeClass(classShow);
			}
			if (!isOnHeader && !isOnForm && !$applyButton.hasClass(classShow)) {
				$applyButton.addClass(classShow);
			}
		})
	}
	// Open window links
	$(document).on('click', '.g-js-open-window', function(e) {
		e.preventDefault();
		window.open($(this).attr('href'),'window','toolbar=no, menubar=no, resizable=yes');
		return false;
	})
	// Print button
	$(document).on('click', '.g-js-print', function(e) {
		e.preventDefault();
		window.print();
		return false;
	})
});