from django.urls import path, include
from django.conf import settings
from common.views import ContactFormView
from .views import HomePageView, CountriesView, DocumentConstanceView
from .forms import MembershipForm, AdvisoryForm, GetAdvisorForm

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('membership/', ContactFormView.as_view(email_to=settings.APP_EMAIL_SECRETARIAT, form_class=MembershipForm), name='membership'),
    path('advisory/', ContactFormView.as_view(email_to=settings.APP_EMAIL_ADVISORY, form_class=AdvisoryForm), name='advisory'),
    path('get_advisor/', ContactFormView.as_view(email_to=settings.APP_EMAIL_ADVISORY, form_class=GetAdvisorForm), name='get_advisor'),
    path('countries/', CountriesView.as_view(), name='countries'),
    path('advisors/', DocumentConstanceView.as_view(content_name='PAGE_ADVISORS', title='Advisors'), name='advisors'),
    path('partners/', DocumentConstanceView.as_view(content_name='PAGE_PARTNERS', title='Partners'), name='partners'),
]

from django.contrib.sitemaps.views import sitemap
from .sitemaps import StaticViewSitemap

sitemaps = {'static': StaticViewSitemap}
urlpatterns_sitemap = [path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),]
urlpatterns += urlpatterns_sitemap
