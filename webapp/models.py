from django.db import models
import time

from django.utils.translation import ugettext_lazy as _

class Party(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('name'))
    slug = models.SlugField(blank=True, verbose_name=_('slug'))
    def __str__(self):
        return self.name    
    class Meta:
        ordering = ('name',)
        verbose_name = _('party')
        verbose_name_plural = _('parties')

class Country(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('name'))
    flag = models.ImageField(blank=True, upload_to='webapp/country/flag')
    def __str__(self):
        return self.name
    class Meta:
        ordering = ('name',)
        verbose_name = _('country')
        verbose_name_plural = _('countries')

class Person(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('person name'))
    description = models.TextField(blank=True, verbose_name=_('description'))
    picture = models.ImageField(upload_to='webapp/person/picture/')
    country = models.ForeignKey(Country, blank=True, null=True, on_delete=models.SET_NULL, verbose_name=_('country'))
    parties = models.ManyToManyField(Party, blank=True)
    link = models.URLField(verbose_name=_('link'), blank=True)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = _('person')
        verbose_name_plural = _('persons')

class CountryDirectored(models.Model):
    country = models.ForeignKey(Country, on_delete=models.CASCADE, verbose_name=_('country'))
    director = models.ForeignKey(Person, on_delete=models.CASCADE, verbose_name=_('director'))
    joined = models.DateField(blank=True, null=True)
    class Meta:
        verbose_name = _('country with director')
        verbose_name_plural = _('countries with director')
        ordering = ('country__name',)

class PartnerAbstarct(models.Model):
    logo = models.ImageField()
    name = models.CharField(max_length=255)
    link = models.URLField(blank=True)
    position = models.PositiveIntegerField(default=0, blank=True)
    class Meta:
        ordering = ('position',)
        abstract = True

class Partner(PartnerAbstarct):
    logo = models.ImageField(upload_to='webapp/partner/logo')
    class Meta(PartnerAbstarct.Meta):
        pass

class Company(PartnerAbstarct):
    logo = models.ImageField(upload_to='webapp/company/logo')
    class Meta(PartnerAbstarct.Meta):
        verbose_name = _('company')
        verbose_name_plural = _('companies')    

class Association(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('name'))
    url = models.URLField(verbose_name=_('link'))
    country = models.ForeignKey(Country, blank=True, null=True, on_delete=models.SET_NULL, verbose_name=_('country'))
    def __str__(self):
        return self.name
    class Meta:
        ordering = ('name',)
        verbose_name = _('association')
        verbose_name_plural = _('associations')    

class Event(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('name'))
    picture = models.ImageField(upload_to='webapp/event/picture/')
    link = models.URLField(blank=True, null=True, verbose_name=_('link'))
    position = models.PositiveIntegerField(default=0, blank=True)
    is_past = models.BooleanField(default=False, blank=True, verbose_name=_('is past'))
    def __str__(self):
        return self.name
    class Meta:
        ordering = ('position',)
        verbose_name = _('event')
        verbose_name_plural = _('events')

class Document(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('name'))
    file = models.FileField(upload_to='webapp/document', verbose_name=_('file'))
    def __str__(self):
        return self.name
    class Meta:
        ordering = ('name',)
        verbose_name = _('document')
        verbose_name_plural = _('documents')