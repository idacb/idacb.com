from django import forms
from django.utils.translation import ugettext_lazy as _

from django.urls import reverse_lazy

from common.forms import Form

membership_choices = (
    ('- not choosen -', 'Choose...'),
    ('IDACB Proffessional', 'IDACB Proffessional'),
    ('IDACB Company', 'IDACB Company'),
    ('IDACB ICO', 'IDACB ICO'),
    ('IDACB Exchange Approved', 'IDACB Exchange Approved'),
    ('IDACB ICO Advisory Board', 'IDACB ICO Advisory Board'),
)

class MembershipForm(Form):
    name = forms.CharField(max_length=255, required=False, label=_("Your name"))
    phone = forms.CharField(max_length=255, label=_("Phone number"))
    email = forms.EmailField(label=_("Email address"))
    social = forms.URLField(required=False, label=_("Facebook or other social network page"))
    organization = forms.CharField(max_length=255, required=False, label=_("Full title with organization"))
    membership = forms.ChoiceField(choices=membership_choices, required=False, label=_("Membership"))

    class Meta(Form.Meta):
        title = 'Membership'
        class_name = 'form-control'
        action = reverse_lazy('membership')
        placeholders = {
            'name': 'John Doe',
            'email': 'johndoe@example.com',
            'phone': '+1 123 456 78 90',
            'social': 'https://www.facebook.com/johndoe',
            'organization': 'CEO, ACME Ltd.'
        }

class AdvisoryForm(Form):
    name = forms.CharField(max_length=255, required=False, label=_("Your name"))
    phone = forms.CharField(max_length=255, label=_("Phone number"))
    email = forms.EmailField(label=_("Email address"))
    organization = forms.CharField(max_length=255, required=False, label=_("Full title with organization"))
    country = forms.CharField(max_length=255, required=False, label=_("The country you represent"))
    project = forms.CharField(max_length=255, required=False, label=_("Project you want to become advisor for"))
    social = forms.URLField(label=_("Your LinkedIn page or other social network page"))
    comment = forms.CharField(required=False, label=_("Your comment"), widget=forms.Textarea)

    class Meta(Form.Meta):
        title = 'Become Advisor'
        class_name = 'form-control'
        action = reverse_lazy('advisory')
        placeholders = {
            'name': 'John Doe',
            'email': 'johndoe@example.com',
            'phone': '+1 123 456 78 90',
            'organization': 'CEO, ACME Ltd.',
            'country': 'United Kingdom',
            'project': 'Opinion ICO',
            'social': 'https://www.linkedin.com/in/johndoe/',
            'comment': 'Your comment if needed...'
        }

class GetAdvisorForm(Form):
    name = forms.CharField(max_length=255, required=False, label=_("Your name"))
    phone = forms.CharField(max_length=255, label=_("Phone number"))
    email = forms.EmailField(label=_("Email address"))
    project = forms.CharField(max_length=255, required=False, label=_("Project you want to get advisor for"))
    comment = forms.CharField(required=False, label=_("Your comment"), widget=forms.Textarea)
    advisor = forms.CharField(required=False, widget=forms.HiddenInput, label=_("Advisor's name"))

    def __init__(self, *args, **kwargs):
        advisor = None
        if 'advisor' in kwargs:
            advisor = kwargs.pop('advisor')
        super().__init__(*args, **kwargs)
        if advisor:
            self.fields['advisor'].initial = advisor.name 

    class Meta(Form.Meta):
        title = 'Get Advisor'
        class_name = 'form-control'
        action = reverse_lazy('get_advisor')
        placeholders = {
            'name': 'John Doe',
            'email': 'johndoe@example.com',
            'phone': '+1 123 456 78 90',
            'project': 'ACME ICO',
            'comment': 'Your comment if needed, project website, etc...'
        }