from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import HttpResponse, Http404
from django.core.mail import send_mail
from django.conf import settings

from constance import config

from common.views import ContactFormView

from .models import Party, Person, Country, Partner, Company, CountryDirectored, Event, Document
from .forms import MembershipForm, AdvisoryForm, GetAdvisorForm

def attach_form_to_advisers(advisor):
    advisor.form = GetAdvisorForm(advisor=advisor)
    return advisor

class HomePageView(TemplateView):
    template_name = 'webapp/home.html'
    def get_context_data(self, **kwargs):
        countries = Country.objects.prefetch_related('association_set').all()
        persons = Person.objects.select_related('country')
        form_membership = MembershipForm()
        form_become_advisor = AdvisoryForm()
        form_get_advisor = GetAdvisorForm()
        partners = Partner.objects.all()
        advisory = list(persons.filter(parties__slug='advisory'))
        advisory = list(map(attach_form_to_advisers, advisory))
        companies = Company.objects.all()
        countries_count = CountryDirectored.objects.all().count()
        events = {
            'upcoming': Event.objects.filter(is_past=False),
            'past': Event.objects.filter(is_past=True),
        }
        parties = {
            'advisory': advisory,
            'trustees': persons.filter(parties__slug='trustees'),
            'secretariat': persons.filter(parties__slug='secretariat'),
            'directors': persons.filter(parties__slug='directors'),
            'professionals': persons.filter(parties__slug='professionals'),
        }
        documents = Document.objects.all()
        return {
            'countries': countries,
            'parties': parties,
            'partners': partners,
            'companies': companies,
            'form_membership': form_membership,
            'form_become_advisor': form_become_advisor,
            'form_get_advisor': form_get_advisor,
            'countries_count': countries_count,
            'events': events,
            'documents': documents
        }

class CountriesView(TemplateView):
    template_name = 'webapp/countries.html'
    def get_context_data(self):
        return {'countries': CountryDirectored.objects.select_related('country', 'director').all()}

class DocumentConstanceView(TemplateView):
    template_name = 'webapp/document_constance.html'
    content_name = 'SOME_CONSTANCE_CONFIG'
    title = ''
    def get_context_data(self):
        return {
            'content': getattr(config, self.content_name),
            'title': self.title
        }