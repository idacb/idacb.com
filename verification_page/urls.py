from django.urls import path, include
from django.conf import settings

from urls import urlpatterns_common

from django.contrib.sitemaps.views import sitemap
from .sitemaps import StaticViewSitemap

from .views import HomePageView
from common.views import ContactFormView
from .forms import VerificationForm

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('apply/', ContactFormView.as_view(email_to=settings.APP_EMAIL_VERIFICATION, form_class=VerificationForm), name='apply'),
]
urlpatterns += urlpatterns_common

sitemaps = {'static': StaticViewSitemap}
urlpatterns_sitemap = [path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap')]
urlpatterns += urlpatterns_sitemap