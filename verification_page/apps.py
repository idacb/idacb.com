from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class VerificationPageConfig(AppConfig):
    name = 'verification_page'
    verbose_name = _('page "verification"')