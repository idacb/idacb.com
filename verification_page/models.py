from django.db import models
from django.utils.translation import ugettext_lazy as _

from webapp.models import PartnerAbstarct

class ICOVerificated(PartnerAbstarct):
    logo = models.ImageField(upload_to='verification_page/ico_verificated/logo')
    class Meta:
        verbose_name = _('Verificated ICO')
        verbose_name_plural = _('Verificated ICOs')