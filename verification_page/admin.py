from django.contrib import admin
from .models import ICOVerificated

@admin.register(ICOVerificated)
class ICOVerificatedAdmin(admin.ModelAdmin):
    list_display = ('name', 'link', 'position')
    list_editable = ('position',)