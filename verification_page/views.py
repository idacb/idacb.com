from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import HttpResponse, Http404
from django.core.mail import send_mail
from django.conf import settings

from .models import ICOVerificated
from .forms import VerificationForm

class HomePageView(TemplateView):
    template_name = 'verification_page/base.html'
    def get_context_data(self):
        icos = ICOVerificated.objects.all()
        return {'icos': icos, 'form': VerificationForm()}