from django import forms
from django.utils.translation import ugettext_lazy as _

from django.urls import reverse_lazy

from common.forms import Form

class VerificationForm(Form):
    name = forms.CharField(max_length=255, required=False, label=_("Your name"))
    phone = forms.CharField(max_length=255, label=_("Phone number"))
    email = forms.EmailField(label=_("Email address"))
    project = forms.URLField(required=False, label=_("Project name"))
    date = forms.CharField(max_length=255, required=False, label=_("Project start date"))
    comment = forms.CharField(required=False, label=_("Your comment"), widget=forms.Textarea)

    class Meta(Form.Meta):
        title = 'Verification'
        class_name = 'form-control'
        action = reverse_lazy('apply')
        placeholders = {
            'name': 'John Doe',
            'email': 'johndoe@example.com',
            'phone': '+1 123 456 78 90',
            'project': 'Acme ICO',
            'date': 'July 2017',
            'comment': 'A few words about the project, project website, etc.'
        }