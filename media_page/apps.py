from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class MediaPageConfig(AppConfig):
    name = 'media_page'
    verbose_name = _('page "media"')