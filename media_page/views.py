from django.shortcuts import render
from django.views.generic import TemplateView

from .models import Partner

class HomePageView(TemplateView):
    template_name = 'media_page/home.html'

class PresentationView(TemplateView):
    def get_context_data(self):
        partners = Partner.objects.all()
        return {'partners': partners}