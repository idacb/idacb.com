from django.urls import path, include
from django.conf import settings

from urls import urlpatterns_common

from .views import HomePageView, PresentationView

from django.contrib.sitemaps.views import sitemap
from .sitemaps import StaticViewSitemap

cryptomedia_template = 'media_page/cryptomedia.html'
blockchainer_template = 'media_page/blockchainer.html'

sitemaps = {'static': StaticViewSitemap}
urlpatterns_sitemap = [path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap')]

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('for-cryptomedia', PresentationView.as_view(template_name=cryptomedia_template), name='cryptomedia'),
    path('for-blockchainer', PresentationView.as_view(template_name=blockchainer_template), name='blockchainer'),
]
urlpatterns += urlpatterns_common
urlpatterns += urlpatterns_sitemap