from django.db import models

class Partner(models.Model):
    logo = models.ImageField(upload_to='media_page/partner/logo')
    name = models.CharField(max_length=255)
    link = models.URLField(blank=True)
    position = models.PositiveIntegerField(default=0, blank=True)

    class Meta:
        ordering = ('position',)