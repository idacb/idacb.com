# Common
mysqlclient
django>=2, <3
gunicorn
django-hosts
django-storages
django-cleanup
django-robots
django-constance[database]

# Debug
django-debug-toolbar

# Galleries
Pillow
sorl-thumbnail

# ?
django-modeltranslation>=0.13b1
django-widget-tweaks
