#!/bin/bash

echo 'Executing entrypoint script...'


# Waiting for database

python <<'EOPYTHON'

import os, sys, time

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')

from django.db import connection

try_number = 0
connected = False
while try_number < 10 and not connected:
    try:
        connection.cursor()
    except:
        print('Not connected to database, waiting...')
    else:
        connected = True
        print('Connected to database...')
    time.sleep(1)
    try_number += 1

if not connected:
    sys.exit(1)

EOPYTHON



# Migrating

python manage.py migrate
python manage.py loaddata Party.json


# Creating superuser and installing fixtures

python <<'EOPYTHON'

import os, sys

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')

import django
from django.contrib.auth import get_user_model

django.setup()

User = get_user_model()
if not User.objects.filter(is_superuser=True).exists():
    User.objects.create_superuser('admin', 'admin@example.com', 'password')
    print('Superuser created...')

# from webapp.models import Party
# if Party.objects.all().count() == 0:
#     pass

EOPYTHON



# Collecting static

yes yes | python manage.py collectstatic



# Running

echo "Running command..."

eval "$@"