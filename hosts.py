from django_hosts import patterns, host

host_patterns = patterns('',
    host(r'', 'urls', name='default'),
    host(r'media', 'media_page.urls', name='media_page'),
    host(r'fund', 'fund_page.urls', name='fund_page'),
    host(r'verification', 'verification_page.urls', name='verification_page'),
    host(r'pricelist', 'pricelist_page.urls', name='pricelist_page'),
    host(r'pitch', 'pitch_page.urls', name='pitch_page'),
    host(r'dashboard', 'dashboard_page.urls', name='dashboard_page'),
)