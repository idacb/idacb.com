from django.db import models
from django.utils.translation import ugettext_lazy as _

class Coach(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('person name'))
    description = models.TextField(blank=True, verbose_name=_('description'))
    picture = models.ImageField(upload_to='pitch_page/coach/picture/')
    link_linkedin = models.URLField(verbose_name=_('linkedin link'), blank=True)
    position = models.PositiveIntegerField(blank=True, verbose_name=_('position'), help_text=_('Leave empty to automatically assign the latest position'))
    def save(self, *args, **kwargs):
        if not self.position:
            latest = self.__class__.objects.order_by('position').last()
            self.position = latest.position + 1 if latest and latest.position else 1
        super().save(*args, **kwargs)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = _('coach')
        verbose_name_plural = _('coaches')

class City(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('city name'))
    picture = models.ImageField(upload_to='pitch_page/city/picture/')
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = _('city')
        verbose_name_plural = _('cities')