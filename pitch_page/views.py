from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import HttpResponse, Http404
from django.core.mail import send_mail
from django.conf import settings

from .models import Coach, City
from .forms import PitchCityForm

def attach_form_to_city(city):
    city.form = PitchCityForm(city=city)
    return city

class HomePageView(TemplateView):
    template_name = 'pitch_page/base.html'
    def get_context_data(self):
        cities = list(City.objects.all())
        cities = list(map(attach_form_to_city, cities))
        return {
            'coaches': Coach.objects.all(),
            'cities': cities,
            'email': settings.APP_EMAIL_PITCH
        }