from django import forms
from django.utils.translation import ugettext_lazy as _

from django.urls import reverse_lazy

from common.forms import Form

class PitchForm(Form):
    name = forms.CharField(max_length=255, required=False, label=_("Your name"))
    phone = forms.CharField(max_length=255, label=_("Phone number"))
    email = forms.EmailField(label=_("Email address"))
    project = forms.URLField(required=False, label=_("Your ICO name"))
    role = forms.URLField(required=False, label=_("Your role in project"))
    comment = forms.CharField(required=False, label=_("What do you need"), widget=forms.Textarea)

    class Meta(Form.Meta):
        title = 'Pitch School'
        class_name = 'form-control'
        action = reverse_lazy('apply')
        placeholders = {
            'name': 'John Doe',
            'email': 'johndoe@example.com',
            'phone': '+1 123 456 78 90',
            'project': 'Acme ICO',
            'role': 'CTO',
            'comment': 'I need to learn ICO pitching features'
        }

class PitchCityForm(PitchForm):
    city = forms.CharField(required=False, widget=forms.HiddenInput, label=_("City"))
    def __init__(self, *args, **kwargs):
        city = None
        if 'city' in kwargs:
            city = kwargs.pop('city')
        super().__init__(*args, **kwargs)
        if city:
            self.fields['city'].initial = city.name 

