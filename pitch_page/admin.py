from django.contrib import admin
from .models import Coach, City

@admin.register(Coach)
class CoachAdmin(admin.ModelAdmin):
    pass

@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    pass