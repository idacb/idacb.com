from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class PitchPageConfig(AppConfig):
    name = 'pitch_page'
    verbose_name = _('page "pitch"')